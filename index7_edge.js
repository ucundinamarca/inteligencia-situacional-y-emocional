/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['-34', '-25', '1085', '699', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo1',
                                type: 'image',
                                rect: ['34px', '25px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo1.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_46',
                                type: 'image',
                                rect: ['34px', '59px', '448px', '149px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_46.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_52',
                                type: 'image',
                                rect: ['210px', '221px', '113px', '113px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_52.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_50',
                                type: 'image',
                                rect: ['103px', '353px', '886px', '300px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_50.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_49',
                                type: 'image',
                                rect: ['333px', '297px', '438px', '332px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_49.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_47',
                                type: 'image',
                                rect: ['877px', '0px', '208px', '227px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_47.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_51',
                                type: 'image',
                                rect: ['0px', '343px', '278px', '356px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_51.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0', '-14', '1039', '654', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo2',
                                type: 'image',
                                rect: ['0px', '14px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo2.png",'0px','0px']
                            },
                            {
                                id: 'home',
                                type: 'image',
                                rect: ['4px', '38px', '76px', '95px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_83.png",'0px','0px']
                            },
                            {
                                id: 'botonera',
                                type: 'group',
                                rect: ['123', '476', '219', '165', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_84',
                                    type: 'image',
                                    rect: ['0px', '0px', '219px', '165px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_84.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_85',
                                    type: 'image',
                                    rect: ['13px', '22px', '28px', '125px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_85.png",'0px','0px']
                                },
                                {
                                    id: 'Group2',
                                    type: 'group',
                                    rect: ['47px', '14px', '156', '137', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn_1',
                                        type: 'image',
                                        rect: ['0px', '0px', '44px', '45px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_88.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_2',
                                        type: 'image',
                                        rect: ['56px', '0px', '44px', '45px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_87.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_3',
                                        type: 'image',
                                        rect: ['111px', '0px', '45px', '45px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_86.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_4',
                                        type: 'image',
                                        rect: ['1px', '46px', '44px', '44px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_94.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_5',
                                        type: 'image',
                                        rect: ['56px', '46px', '44px', '44px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_91.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_6',
                                        type: 'image',
                                        rect: ['111px', '46px', '45px', '44px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_89.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_7',
                                        type: 'image',
                                        rect: ['1px', '93px', '44px', '44px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_93.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_8',
                                        type: 'image',
                                        rect: ['56px', '93px', '44px', '44px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_92.png",'0px','0px'],
                                        userClass: "botonones"
                                    },
                                    {
                                        id: 'btn_9',
                                        type: 'image',
                                        rect: ['111px', '93px', '45px', '44px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_90.png",'0px','0px'],
                                        userClass: "botonones"
                                    }]
                                }]
                            },
                            {
                                id: 'Recurso_96',
                                type: 'image',
                                rect: ['891px', '233px', '148px', '144px', 'auto', 'auto'],
                                clip: 'rect(0px 121px 111px 26px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_96.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'Recurso_97',
                                type: 'image',
                                rect: ['887px', '343px', '148px', '144px', 'auto', 'auto'],
                                clip: 'rect(0px 118px 112px 26px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_97.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'Recurso_98',
                                type: 'image',
                                rect: ['932px', '161px', '58px', '73px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_98.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'Recurso_1012',
                                type: 'image',
                                rect: ['696px', '476px', '122px', '21px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1012.png",'0px','0px']
                            },
                            {
                                id: 'box1',
                                type: 'rect',
                                rect: ['354px', '523px', '322px', '113px', 'auto', 'auto'],
                                fill: ["rgba(192,192,192,0)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'box2',
                                type: 'rect',
                                rect: ['694px', '523px', '322px', '113px', 'auto', 'auto'],
                                fill: ["rgba(192,192,192,0)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 's1',
                                type: 'rect',
                                rect: ['486px', '472px', '197px', '29px', 'auto', 'auto'],
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 's2',
                                type: 'rect',
                                rect: ['822px', '472px', '197px', '29px', 'auto', 'auto'],
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Recurso_1002',
                                type: 'image',
                                rect: ['354px', '476px', '131px', '21px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1002.png",'0px','0px']
                            },
                            {
                                id: 'box',
                                type: 'group',
                                rect: ['110', '110', '781', '366', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_81',
                                    type: 'image',
                                    rect: ['0px', '45px', '781px', '321px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_81.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_95',
                                    type: 'image',
                                    rect: ['54px', '276px', '656px', '74px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_95.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_82',
                                    type: 'image',
                                    rect: ['10px', '0px', '197px', '55px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_82.png",'0px','0px']
                                },
                                {
                                    id: 'Text3',
                                    type: 'text',
                                    rect: ['27px', '71px', '715px', '189px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "justify",
                                    font: ['Arial, Helvetica, sans-serif', [16, "px"], "rgba(43,71,139,1.00)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'Recurso_99',
                                type: 'image',
                                rect: ['613px', '0px', '414px', '148px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_99.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['-26px', '-28', '1088', '702', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidad',
                                type: 'image',
                                rect: ['25px', '28px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_74',
                                type: 'image',
                                rect: ['226px', '141px', '595px', '410px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_74.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_75',
                                type: 'image',
                                rect: ['782px', '124px', '56px', '55px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_75.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_79',
                                type: 'image',
                                rect: ['842px', '0px', '246px', '269px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_79.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_80',
                                type: 'image',
                                rect: ['0px', '346px', '278px', '356px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_80.png",'0px','0px']
                            },
                            {
                                id: 'creditos',
                                type: 'rect',
                                rect: ['250px', '166px', '546px', '332px', 'auto', 'auto'],
                                overflow: 'auto',
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"],
                                c: [
                                {
                                    id: 'Text2',
                                    type: 'text',
                                    rect: ['9px', '9px', '513px', '328px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-size: 12px;\">Nombre del material</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Inteligencia Emocional</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Integrantes Celda de Producción - RED</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">&nbsp;&nbsp;&nbsp;Lida Consuelo Rincón Méndez</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Diseño instruccional</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Sonia Smith Niño</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​Experto en Contenido</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Nazly María Victoria Díaz Vera</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Diseño gráfico</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Edilson Laverde Molina</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Programación</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Las imágenes empleadas han sido tomadas de https://www.freepik.es</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Imágenes</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">&nbsp;Lida Consuelo Rincón Méndez</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Voz</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">​</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Oficina de Educación Virtual y a Distancia</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">Universidad de Cundinamarca</span></p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 12px;\">2021</span></p><p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "12px", "", "none"]
                                }]
                            },
                            {
                                id: 'objetivo',
                                type: 'rect',
                                rect: ['258px', '179px', '531px', '332px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"],
                                c: [
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['11px', '12px', '509px', '243px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-weight: 800;\">Objetivo&nbsp;&nbsp;</span></p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 100; font-size: 20px;\">Implementar técnicas y estrategias de control emocional para una adecuada gestión de las emociones.&nbsp;</span></p><p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'instrucciones',
                                type: 'rect',
                                rect: ['258px', '179px', '531px', '332px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"],
                                c: [
                                {
                                    id: 'TextCopy2',
                                    type: 'text',
                                    rect: ['11px', '-3px', '509px', '243px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-weight: 800;\">Instrucción</span></p><p style=\"margin: 0px; text-align: justify;\"><span style=\"font-size: 20px;\">&nbsp;</span></p><p style=\"margin: 0px; text-align: justify;\"></p><p style=\"margin: 0px; text-align: justify;\"></p><p style=\"margin: 0px; text-align: justify;\">Lea detenidamente cada situación y dé clic en la flecha para elegir las técnicas y/o estrategias empleadas por los sujetos para controlar sus emociones. Finalmente, dé clic en los botones agregar.<br></p><p style=\"margin: 0px; text-align: justify;\">​</p><p style=\"margin: 0px; text-align: justify;\">Con el fin de realizar con éxito la presente actividad, le sugerimos realizar la lectura del artículo “Control emocional y estrategias”, escrito por Ivonne Maritza Ibarra Pinilla.</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\"><span style=\"font-weight: 100;\"></span></p><p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'retro',
                                type: 'group',
                                rect: ['440', '179', '185', '74', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_77',
                                    type: 'image',
                                    rect: ['0px', '0px', '84px', '74px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_77.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_78',
                                    type: 'image',
                                    rect: ['101px', '0px', '84px', '74px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_78.png",'0px','0px']
                                },
                                {
                                    id: 'Text4',
                                    type: 'text',
                                    rect: ['-131px', '106px', '427px', '157px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "justify",
                                    font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index7_edgeActions.js");
})("EDGE-32963756");
