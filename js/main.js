
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Inteligencia situacional y emocional",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"  },
    {url:"sonidos/fail.mp3",       name:"fail"  },
    {url:"sonidos/good.mp3",       name:"good"  },
    {url:"sonidos/intro.mp3",      name:"intro" },
    {url:"sonidos/intro1.mp3",     name:"intro1" },
    {url:"sonidos/intro2.mp3",     name:"intro2" },
    {url:"sonidos/intro3.mp3",     name:"intro3" },
];
var module="g0";
var good2=0;

var index=0;
function main(sym) {

var estrategia=[
  
    {
        text:"Estrategia de visualización"
    },
    {
        text:"Estrategias a partir de conductas propias"
    },
    {
        text:"Estrategia de comunicación personal"
    },
   
    {
        text:"Estrategia de cambio de actividad"
    },
    {
        text:"Estrategia a partir de nuevas conductas" 
    },
    {
        text:"Estrategias relacionadas con la mente"
    },
    {
        text:"Estrategia de afrontamiento activo"
    }
];

var tecnicas=[
    {
        text:"Técnica de relajación muscular"
    },
    {
        text:"Técnica de cambio de actividad"
    }
];
var vistos=[0,0,0,0,0,0,0,0,0];
var goods= [1,1,1,1,1,1,1,1,1];
items=[
    {
        situacion:"Mariana es la secretaria de Alberto Corredor. Ella ha trabajado con él los últimos cinco (5) años y durante todo este tiempo ha soportado su temperamento irascible y demandante, pero el día de hoy tuvieron una discusión fuerte, en la que él la trató mal y la amenazó con su trabajo. Con el propósito de evitar más enfrentamientos, Mariana salió de la oficina, caminó un poco y bebió un café, para luego regresar a su sitio de trabajo más tranquila.  <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 2 técnicas",
        rta:["Técnica de relajación muscular","Técnica de cambio de actividad"]
    },
    {
        situacion:"Juan Andrés es un funcionario de la Oficina de Talento Humano, y el día de hoy tuvo un inconveniente con un funcionario nuevo, quién no ha entregado su documentación completa, lo cual es necesario para legalizar el contrato.  Con el fin de obtener la información requerida, Juan Andrés le ha llamado con insistencia por varios días, pero éste no le ha respondido. Hoy, a primera hora, llegó el nuevo funcionario a la oficina, y de forma beligerante y grosera reclamó a Juan Andrés por sus insistentes llamadas. Juan Andrés, por su parte, permaneció en silencio, inició un diálogo interno, positivo y tranquilizador, luego imaginó estar en un lugar tranquilo y apacible, y cuando estuvo más calmado, solicitó a un compañero atender al funcionario en mención. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 1 técnica y 2 estrategias ",
        rta:["Técnica de relajación muscular","Estrategias relacionadas con la mente","Estrategia de visualización"]
    },
    {
        situacion:"Ángela es secretaria de contaduría en su organización y debido a la pandemia está realizando sus labores en modalidad teletrabajo, por lo cual permanece en casa todo el día, todos los días de la semana; lo cual ha hecho que luego de un año de encierro y pocos fines de semana libres debido al exceso de trabajo, Ángela permanezca irascible, estresada y desanimada por el rumbo que ha tomado su vida hasta el momento. Debido a algunos enfrentamientos acaecidos con sus compañeros de oficina, Ángela decidió visitar al psicólogo, quien le aconsejó realizar alguna actividad que le guste, como la lectura, el dibujo, etc. En caso de estar irascible, estresada o triste, le recomendó cambiar a una de estas actividades para detener ese tipo de sentimientos negativos. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 2 estrategias",
        rta:["Estrategia de cambio de actividad","Estrategias a partir de conductas propias"]
    },
    {
        situacion:"Amelia es contadora en su organización, ha enfrentado varios problemas emocionales últimamente debido al fallecimiento de su padre, lo cual ha hecho que se manifieste distraída, triste, ansiosa y que, adicionalmente, cometa varios errores en su trabajo; situación que la motivó a visitar al psicólogo en los últimos días, quien le aconsejó ingresar a una terapia para superar su duelo, en la que pueda hablar y manifestar sus emociones, y realizar una actividad que le ayude a mantener un estado de ánimo positivo, como pintar o realizar una actividad física. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 2 estrategias",
        rta:["Estrategias a partir de conductas propias","Estrategia a partir de nuevas conductas"]
    },
    {
        situacion:"Alberto, ingeniero de soporte en la Oficina de Sistemas, ha estado experimentando niveles muy bajos de azúcar, lo cual hace que tenga problemas para manejar su ira. Debido a ello, y a que como parte de su trabajo debe atender al público, su médico de cabecera le ha aconsejado hablar de forma pausada y sin prisa, regular el ritmo de su respiración y, siempre que se torne muy irascible, buscar un lugar para sentarse, relajarse y hacer ejercicios de respiración consciente. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 1 técnica y 1 estrategia",
        rta:["Estrategia de visualización","Técnica de relajación muscular"]
    },
    {
        situacion:"Cristina ha tenido un volumen exagerado de trabajo durante este tiempo de pandemia, lo cual ha hecho que pierda el control con facilidad y se enfrasque en discusiones inútiles con su jefe, quien, sin notar el cansancio de su subalterna, exige cada vez más y más. Debido a esto, Cristina ha optado por guardar silencio para evitar enfrentamientos y cambiar de actividad con el propósito de dejar atrás los pensamientos negativos y su frustración por no tener tiempo suficiente para descansar y compartir con su familia. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 2 estrategias",
        rta:["Estrategia de cambio de actividad","Estrategia a partir de nuevas conductas"]
    },
    {
        situacion:"Marcela maneja un equipo de trabajo de veinte (20) personas, y al final del día no le es fácil reducir el estrés o tener un buen estado de ánimo, por lo cual ha decidido ingresar al gimnasio y hacer actividad física cuatro (4) veces por semana. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 1 estrategia",
        rta:["Estrategia de afrontamiento activo"]
    },
    {
        situacion:"Ana perdió a su esposo hace 3 años, pero a pesar de que ha pasado el tiempo, aún enfrenta momentos de depresión fuerte y no puede evitar la sensación de soledad, por lo cual su médico le ha aconsejado meditar y realizar alguna actividad física, y cuando enfrente periodos depresivos, cambiar de actividad.  <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 3 estrategias",
        rta:["Estrategia de afrontamiento activo","Estrategia a partir de nuevas conductas","Estrategias relacionadas con la mente"]
    },
    {
        situacion:"Juliana, es diseñadora gráfica y debido al incremento del aprendizaje en línea, la empresa en la que labora ha empezado a producir gran cantidad de material digital, lo cual ha aumentado su volumen de trabajo de manera exponencial y exagerada, lo que le genera niveles muy altos de estrés,  que le han provocado problemas graves de salud, por lo que su médico le ha aconsejado ceñirse a sus horarios de trabajo, realizar actividad física, meditar, hacer pausas activas y realizar ejercicios de respiración durante el día. <b>Para responder acertadamente, usted debe elegir técnicas y/o estrategias según considere.</b> Seleccione 1 técnica y 2 estrategias",
        rta:["Técnica de relajación muscular","Estrategias relacionadas con la mente","Estrategia de afrontamiento activo"]
    }
];
var t=null;
udec = ivo.structure({
        created:function(){
           t=this;
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               ivo(ST+"preload").hide();
               t.events();
               t.animation();
               stage1.play();

           });
        },
        methods: {
            
            select:function(){
                var analitic=function(){
                    let correct=true;
                    let points=0;
                    for (i of items){
                        if(i.rta!=false){
                            for (o of i.rta){
                                if(o==false){
                                    correct=false;
                                }
                                if(o==true){
                                    correct=true;
                                }
                                if(o!=true && o!=false){
                                    correct=false;
                                }
                            }
                        }else{
                            correct=false;
                        }
                        if (correct){
                            points+=1;
                        }
                    }
                    return points;
                }
                var validacion=function(msg){
                    let id = $(ST+"Text3").attr("data-index");
                    let good=false;
                    if(items[id].rta != false){
                        for(const [i,r] of items[id].rta.entries()){
                            if(msg.toString()==r.toString()){
                                items[id].rta[i]=true;
                                good=true;
                            }
                        }
                        //'Estrategia de visualización'==='Estrategia de visualización'
                        if(good){
                            ivo.play("good");
                            return "good";
                        }else{
                            items[id].rta=false;
                            ivo.play("fail");
                            return "fail";
                        }
                    }
                }
                $(ST+"s1").html(`<select style="width:100%;background:#2B478B;color:#FFF; height:32px;border:0;" id="s1"></select><button id="add1" style="cursor:pointer;border:0;float:right;width:50%;background:#2B478B;color:#FFF; height:32px;">Agregar</button>`);
                $(ST+"s2").html(`<select style="width:100%;background:#2B478B;color:#FFF; height:32px;border:0;" id="s2"></select><button id="add2" style="cursor:pointer;border:0;float:right;width:50%;background:#2B478B;color:#FFF; height:32px;">Agregar</button><br><br><br><br><br><br><br><button id="finalizar" style="cursor:pointer;border:0;float:right;width:50%;background:#2B478B;color:#FFF; height:32px;display:none;">Finalizar</button>`);
                $(ST+"box1").html(`<ul id="l1" ></ul>`);
                $(ST+"box2").html(`<ul id="l2" ></ul>`);
                let n=0;
                for(v of vistos){
                    if(v==1){
                        n+=1;
                    }
                }
    
                if(n==8){
                    $("#finalizar").show();
                }
                for(t of tecnicas){ id="add1"
                    $("#s1").append(`<option value="${t.text}">${t.text}</option>`);
                }
                for(t of estrategia){
                    $("#s2").append(`<option value="${t.text}">${t.text}</option>`);
                }
                $("#add1").on("click",function(){
                    if($("#s1").val()!=null){
                        $("#l1").append(`<li class="${validacion($("#s1").val())}">${$("#s1").val()}</li>`);
                        $('#s1 option[value="'+$("#s1").val()+'"').remove();
                    }
                });
                $("#add2").on("click",function(){
                    if($("#s2").val()!=null){
                        $("#l2").append(`<li class="${validacion($("#s2").val())}">${$("#s2").val()}</li>`);
                        $('#s2 option[value="'+$("#s2").val()+'"').remove();
                    }
                });
                $("#finalizar").on("click",function(){
                    let n=analitic();
                    let msg='';
                    let nota=0;
                    if(n<=2 ){
                        msg='¡Ánimo, siga intentándolo! Es importante revisar los recursos educativos del curso para fortalecer sus conocimientos.';
                        if(n>0){
                            nota=20;
                        }
                    }
                    if(n==3 || n==4 ){
                        msg='Ha sido un buen esfuerzo. Sin embargo, es necesario revisar los recursos educativos del curso para construir conocimientos nuevos.';
                        nota=25;
                    }
                    if(n==5 || n==7 ){
                        msg='Muy bien, ha sido un buen trabajo.';
                        nota=40;
                    }
                    if(n==8 || n==9 ){
                        msg='Excelente, felicidades! Conoce muy bien las estrategias y técnicas para el control de las emociones.';
                        nota=50;
                    }
                    ivo(ST+"Text4").html(`${msg}<br><p>Nota: ${nota}</p>`);
                    stage31.play();
                    Scorm_mx = new MX_SCORM(false);
                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+" Nota: "+nota);
                    Scorm_mx.set_score(nota);
                });
            },
            events:function(){
                var t=this;
                $(ST+"Recurso_52").on("click",function(){
                    stage1.reverse().timeScale(10);
                    stage2.play().timeScale(1);
                    ivo.play("clic");
                    $(this).removeClass("animated infinite flash");
                });
                $(ST+"home").on("click",function(){
                    stage2.reverse().timeScale(10);
                    stage1.play().timeScale(1);
                    ivo.play("clic");
                    $(this).removeClass("animated infinite flash");
                });

                $(ST+"Recurso_98").on("click",function(){
                    stage31.reverse().timeScale(10);
                    stage33.reverse().timeScale(10);
                    stage32.play().timeScale(1);
                    ivo.play("clic");
                });
                $(ST+"Recurso_97").on("click",function(){
                    stage31.reverse().timeScale(10);
                    stage32.reverse().timeScale(10);
                    stage33.play().timeScale(1);
                    ivo.play("clic");
                });

                $(ST+"Recurso_96").on("click",function(){
                    stage31.reverse().timeScale(10);
                    stage32.reverse().timeScale(10);
                    stage33.reverse().timeScale(10);
                    stage34.play().timeScale(1);
                    ivo.play("clic");
                });
                $(ST+"Recurso_75").on("click",function(){
                    stage31.reverse().timeScale(10);
                    stage32.reverse().timeScale(10);
                    stage33.reverse().timeScale(10);
                    stage34.reverse().timeScale(10);
                    ivo.play("clic");
                });
                $(".botonones").on("click",function(){
                    let id=parseInt($(this).attr("id").split(S+"btn_")[1])-1;

                    $(this).css("opacity",0.5);
                    if($(ST+"Text3").attr("data-index")!=null){
                        vistos[$(ST+"Text3").attr("data-index")]=1;
                    }
                    if($(ST+"Text3").attr("data-index")==null || vistos[id]==0){
                        $(ST+"Text3").html(items[id].situacion);
                        $(ST+"Text3").attr("data-index",id);
                        ivo.play("clic");
                        t.select();
                    }
                });

            },
            animation:function(){
                stage1 = new TimelineMax({onComplete:function(){
                    $(ST+"Recurso_52").addClass("animated infinite flash");
                }});
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_46", .8,      {x:-1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_52", .8,      {x:-300,opacity:0}), 0); 
                stage1.append(TweenMax.from(ST+"Recurso_49", .8,      {y:-300,opacity:0}), 0);
                stage1.append(TweenMax.to(ST+"Recurso_49", .8,        {y:-30,opacity:0.8}), 0);
                stage1.append(TweenMax.to(ST+"Recurso_49", .8,        {y:0,opacity:1}), 0);
                stage1.stop();

                stage2 = new TimelineMax({
                    onComplete:function(){
                        $(ST+"btn_1").trigger("click");
                    }
                });
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_99", .8,      {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"box", .8,             {y:-300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"botonera", .8,        {y:300,opacity:0}), 0);
                stage2.append(TweenMax.staggerFrom(".btn",.4,         {x:300,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.append(TweenMax.from(ST+"home", .8,            {y:300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_1002", .8,    {y:300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_1012", .8,    {y:300,opacity:0}), 0);
                stage2.stop();

                stage31 = new TimelineMax();
                stage31.append(TweenMax.fromTo(ST+"stage3", .4,       {opacity:0,y:900},{opacity:1,y:0}), 0); 
                stage31.append(TweenMax.from(ST+"retro", .8,          {opacity:0}), 0);
                stage31.stop();

                stage32 = new TimelineMax();
                stage32.append(TweenMax.fromTo(ST+"stage3", .4,       {opacity:0,y:900},{opacity:1,y:0}), 0); 
                stage32.append(TweenMax.from(ST+"creditos", .8,       {opacity:0}), 0);
                stage32.stop();

                stage33 = new TimelineMax();
                stage33.append(TweenMax.fromTo(ST+"stage3", .4,       {opacity:0,y:900},{opacity:1,y:0}), 0); 
                stage33.append(TweenMax.from(ST+"objetivo", .8,       {opacity:0}), 0);
                stage33.stop();

                stage34 = new TimelineMax();
                stage34.append(TweenMax.fromTo(ST+"stage3", .4,       {opacity:0,y:900},{opacity:1,y:0}), 0); 
                stage34.append(TweenMax.from(ST+"instrucciones", .8,  {opacity:0}), 0);
                stage34.stop();

            }
        }
 });
}
